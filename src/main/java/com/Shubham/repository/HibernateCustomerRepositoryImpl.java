package com.Shubham.repository;

import java.util.ArrayList;
import java.util.List;

import com.Shubham.model.Customer;

public class HibernateCustomerRepositoryImpl implements CustomerRepository {

	
	@Override
	public List<Customer> findAll() {
		List<Customer> customers = new ArrayList<>();
		
		Customer customer = new Customer();
		
		customer.setFirstname("Shubham");
		customer.setLastname("Hansen");
		
		customers.add(customer);
		
		return customers;
	}
}
